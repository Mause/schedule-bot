# schedule-bot

Discord bot for schedule reminders

## Usage
- Bot will pull json schedule from /somewhere/ and provide alerts an hour before and 15 minutes before.
- Mappings from room names in schedule to discord channel IDs in `channel_map.json`.
- API tokens are stored in `api_tokens.json`.

```usage: schedule-bot.py [-h] [-U] [-f FILE] [-c] [-n] [-t TIME]

Discord schedule bot

optional arguments:
  -h, --help            show this help message and exit
  -U, --update          Update schedule from url before running
  -f FILE, --file FILE  Use schedule file
  -c, --config          Show configuration
  -n, --noconnect       Don't connect to discord
  -t TIME, --time TIME  Set start time for first event with offset in minutes
```
