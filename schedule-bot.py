#! /usr/bin/env python

# Discord schedule bot

import json, asyncio, discord, argparse, copy, requests
from discord.utils import get
from discord.ext.tasks import loop
from discord.ext.commands import MemberConverter
from datetime import datetime, timezone, timedelta

class bot(discord.Client):
	def __init__(self, config, schedule, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.config = config
		self.schedule = schedule
		self.channels = {}

	async def on_ready(self):
		print('Logged on as', self.user)
		await client.change_presence(activity=discord.Game(name='the game'))
		for channel in self.config.channel_map:
			self.channels[channel] = self.get_channel(int(self.config.channel_map[channel]))
		print(self.channels)
		self.check_time.start()

	async def tagSpeakers(self, users, speakers):
		tags = []
		s = copy.deepcopy(speakers)

		for user in users:
			if str(user.nick) in s: #Tag speakers if nickname found
				tags.append("<@{0}>".format(user.id))
				s.remove(user.nick)

		tags.extend(s) #Add speakers we didn't find

		return ', '.join(tags)

	## TODO: Implement this
	async def emailSpeakers(self, speakerCodes, event):
		pass

	# Gets current unix time, search dict for time, trigger messaging if found
	# Is this a terrible way to do it?
	@loop(seconds=1)
	async def check_time(self):
		index = int(datetime.now().timestamp())
		delta60m = timedelta(minutes=60).seconds
		delta15m = timedelta(minutes=15).seconds

		events60m = self.schedule.eventsAtTime(index+delta60m)
		events15m = self.schedule.eventsAtTime(index+delta15m)

		if(len(events60m) > 0 or len(events15m) > 0):
			users = self.get_all_members()

			if(len(events60m) > 0):
				for event in events60m:
					# for user in users:
					# 	print(user.nick)
					string = "({0})  ({1}) <{2}> - <{3}> - We are ready for your Tech Check, please confirm you are here!".format(
						event["room"], datetime.fromtimestamp(index+delta60m), await self.tagSpeakers(users, event["speakers"]) , event["title"])
					print("Sending ---{0}--- to speaker-checkin".format(string))
					await self.channels["speaker-checkin"].send(string) # speaker-checkin

			if(len(events15m) > 0):
				for event in events15m:
					#Should check if talk is pre-recorded here, once data is available
					speakers = ""
					for speaker in event["speakers"]:
						speakers += speaker + " "

					string = "({0})  ({1}) <{2}> - <{3}> - We are almost ready to invite you to your stream. Please confirm you are here!".format(
						event["room"], datetime.fromtimestamp(index+delta15m), await self.tagSpeakers(users, event["speakers"]), event["title"])
					print("Sending ---{0}--- to {1}".format(string,event["room"]))
					await self.channels[event["room"]].send(string) # in relevant green room`

class scheduleWrangler():
	def __init__(self, filename="schedule.json",  url=None):
		self.events = {}
		self.url = url
		self.filename = filename
		self.ImportTalks()

	# Grabs all results for api query recursively
	def getResults(self, url):
		results = []
		print("Get {0}".format(url))
		response = requests.get(url).json()
		results.extend(response["results"])

		if(response["next"] != None):
			results.extend(self.getResults(response["next"]))
		return results

	def ImportTalks(self):
		scheduleJSON = ""
		results = []

		#prefer url over filename
		if self.url == None:
			print("Using file: {0}".format(conf.schedule))
			with open(conf.schedule) as s:
				results = json.load(s)["results"]
		else:
			print("Ignoring file, pulling from API")
			results = self.getResults(self.url + "?format=json&state=confirmed")

		print("loaded {0} talks.".format(len(results)))

		#Should have data here, bail if we don't
		if(len(results) == 0):
			sys.exit(-1)


		self.events.clear() #Erase previous data if it existed
		for event in results:
			startTime = int(datetime.fromisoformat(event["slot"]["start"]).timestamp())
			if startTime not in self.events:
				self.events[startTime] = []

			#Need (room name)  (time-of-talk) <speaker name> - <talk name>>
			# Build internal data structure
			e = {}
			e["title"] = event["title"]
			e["speakers"] = []
			e["speakerCodes"] = []
			for speaker in event["speakers"]:
				e["speakers"].append(speaker["name"])
				e["speakerCodes"].append(speaker["code"])
			e["room"] = event["slot"]["room"]["en"]
			self.events[startTime].append(e)

	def eventsAtTime(self, time):
		events = []
		if time in self.events:
			events = self.events[time]
		return events

class configuration():
	def __init__(self, schedule="schedule.json"):
		self.tokens = {}
		self.channel_map = {}
		with open("api_tokens.json") as f:
			self.tokens = json.load(f)
		with open("channel_map.json") as f:
			self.channel_map = json.load(f)
		with open("pretalx_endpoints.json") as f:
			self.endpoints = json.load(f)
		self.schedule = schedule

	def show(self):
		print("Tokens:\n{0} \nChannel Map:\n{1}\nSchedule file: {2}".format(self.tokens,self.channel_map,self.schedule))

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Discord schedule bot")
	parser.add_argument('-U', '--update', action="store_true", help="Update schedule from url before running")
	parser.add_argument('-f', '--file', action="store", help="Use schedule file")
	parser.add_argument('-c', '--config', action="store_true", help="Show configuration")
	parser.add_argument('-n', '--noconnect', action="store_true", help="Don't connect to discord")
	parser.add_argument('-t', '--time', action="store", help="Set start time for first event with offset in minutes")
	args = parser.parse_args()

	conf = configuration() if args.file == None else configuration(schedule=args.file)

	if(args.config):
		conf.show()

	if(args.update):
		schedule = scheduleWrangler(url=conf.endpoints["Schedule"])
	else:
		schedule = scheduleWrangler(filename=conf.schedule)

	# For debugging
	if(not args.time == None):
		first_event = list(schedule.events.keys())[0] #grab first key
		#Add fist event with key=now+offset
		schedule.events[int(datetime.now().timestamp())+timedelta(minutes=float(args.time)).seconds] = schedule.events[first_event]

	if not args.noconnect:
		client = bot(conf, schedule)
		client.run(conf.tokens["discordToken"])
